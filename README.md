This repo contains some of my notes and learnings. As I go through and clean things up, I may publish some of these more formally. For now, it is just a way to keep backups and share with those interested.

Here are some Google Collab links to some of the jupyter notebooks included, as this will be a more user-friendly and interactive way to explore this content:

Notebook | Notes
--- | --- | ---
[A/B Testing notes](https://colab.research.google.com/drive/1OXG-5vcRoKxgrH5rkp_WFoaMisYzcs6i?usp=sharing) | Notes on the stats behind controlled randomized testing
[twitter api exploration](https://drive.google.com/file/d/1narpp7eab8Sb_gQ7RhgJzFgGCLxS-bbd/view?usp=sharing) | Also a component in an upcoming project 
[movie clip extractor](https://drive.google.com/file/d/1EisT5ZNlwm1qPmY0cpgbaXApr1z1JSAi/view?usp=sharing) | Utility I made to help my wife
[barchart race visualization](https://drive.google.com/file/d/1y8hyrmZUawl9lVlRq1e1RMjJkE6dByPY/view?usp=sharing) | Neat datavis concept for time-varying data
[gantt timeline](https://drive.google.com/file/d/1S5450lO6n-Sk48Z1SohvPVwyf_DeDpIN/view?usp=sharing) | Matplotlib Gantt plotter
[wordcloud generator](https://drive.google.com/file/d/188lnZlt-zg-X83IZEVllXplwq82QMAk-/view?usp=sharing) | Creates a wordcloud from text